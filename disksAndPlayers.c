#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "disksAndPlayers.h"

//Large section of code was taken from Moodle.

void initializePlayers(player player1, player player2){
    int nameSize;
    // Insert player 1
    printf("Player 1 please insert your name:   \n");
    fgets(player1.name, 20, stdin);
    nameSize = strlen(player1.name);
    player1.name[nameSize -1] = '\0';

    // Assign colours and points to player 1
    player1.type = BLACK;
    player1.points = 2;

    // Insert player 2
    printf("Player 2 please insert your name:   \n");
    fgets(player2.name, 20, stdin);
    nameSize = strlen(player2.name);
    player2.name[nameSize -1] = '\0';

    // Assign colours and points to player 2
    player2.type = WHITE;
    player2.points = 2;

}

void initializeBoard(disk board [SIZE][SIZE]){
 int i, j;
 //board initialization
    for(i=0; i< SIZE; i++){
        for(j=0;j<SIZE; j++){
            if(i==3){
                if(j==3)
                    board[i][j].type = WHITE;
                else{
                    if(j==4)
                        board[i][j].type = BLACK;
                    else
                        board[i][j].type = NONE;
                    }
                }
            else {
                if(i==4){
                    if(j == 3)
                        board[i][j].type = BLACK;
                    else {
                        if(j == 4)
                        board[i][j].type = WHITE;
                        else
                            board[i][j].type = NONE;
                        }
                    }
                else
                    board[i][j].type = NONE;
            }
            board[i][j].pos.row = i;
            board[i][j].pos.col = j;

            }
        }
    }


void printBoard(disk board[SIZE][SIZE]){
    int i,j;
    j = 0;

    printf("\n    ");
    for(i=0; i< SIZE; i++){
        printf("%d   ",i);
    }
    for(i=0; i< SIZE; i++){
        printf("\n%d | ", i);
        for(j=0;j<SIZE; j++){
            switch(board[i][j].type){
                case BLACK:
                    printf("1 | ");
                    break;
                case WHITE:
                    printf("0 | ");
                    break;
                case NONE:
                    printf("x | ");
                    break;
                default:
                    break;
            }
        }
    }

}

/*     BLACK and WHITE had very similar functionality therefore I commented on only black.
       There were issues with move functionality which I will explore in the README text file.
       All functions are present in program but game functions only consist of left, right, and down.       */

void makeMove(disk board[SIZE][SIZE]){

  int a, b , i, j ;

  printf("\nAvailable moves for player 1:\n"); //Available moves printed out for black / 1
  for (i = 0;i<SIZE; i++){
    for (j=0; j<SIZE; j++){
      switch(board[i][j].type){
        case NONE:
        break;

        case BLACK:

        a = i;
        b = j;

            if (board[a][b].type == BLACK){
              if (board[a][b+1].type == NONE){
            }else {
                if (board[a][b+1].type == WHITE)
                if (board[a][b+2].type == NONE)
                printf("%d,%d\n",a, b+2 );
                else {
                  if (board[a][b+1].type == WHITE)
                  if (board[a][b+2].type == WHITE)
                  if (board[a][b+3].type == NONE)
                  printf("%d, %d\n",a, b+3);
} } }

if (board[a][b].type == BLACK){
  if (board[a-1][b].type == NONE){

}else {
    if (board[a-1][b].type == WHITE)
    if (board[a-2][b].type == NONE)
    printf("%d,%d\n",a-2, b );
    else {
      if (board[a-1][b].type == WHITE)
      if (board[a-2][b].type == WHITE)
      if (board[a-3][b].type == NONE)
      printf("%d, %d\n",a-3, b );

} } }
            if (board[a][b].type == BLACK){
              if (board[a+1][b].type == NONE){

            }else {
                if (board[a+1][b].type == WHITE)
                if (board[a+2][b].type == NONE)
                printf("%d,%d\n",a+2, b );
                else {
                  if (board[a+1][b].type == WHITE)
                  if (board[a+2][b].type == WHITE)
                  if (board[a+3][b].type == NONE)
                  printf("%d, %d\n",a+3, b );
} } }

if (board[a][b].type == BLACK){
  if (board[a][b-1].type == NONE){

}else {
    if (board[a][b-1].type == WHITE)
    if (board[a][b-2].type == NONE)
    printf("%d,%d\n",a, b-2 );
    else {
      if (board[a][b-1].type == WHITE)
      if (board[a][b-2].type == WHITE)
      if (board[a][b-3].type == NONE)
      printf("%d, %d\n",a, b-3 );
} } }

if (board[a][b].type == BLACK){
  if (board[a-1][b+1].type == NONE){

}else {
    if (board[a-1][b+1].type == WHITE)
    if (board[a-2][b+2].type == NONE)
    printf("%d,%d\n",a-2, b+2 );
    else {
      if (board[a-1][b+1].type == WHITE)
      if (board[a-2][b+2].type == WHITE)
      if (board[a-3][b+3].type == NONE)
      printf("%d, %d\n",a-3, b+3 );

} } }

if (board[a][b].type == BLACK){
if (board[a+1][b+1].type == NONE){

}else {
if (board[a+1][b+1].type == WHITE)
if (board[a+2][b+2].type == NONE)
printf("%d,%d\n",a+2, b+2 );
else {
if (board[a+1][b+1].type == WHITE)
if (board[a+2][b+2].type == WHITE)
if (board[a+3][b+3].type == NONE)
printf("%d, %d\n",a+3, b+3 );

} } }

if (board[a][b].type == BLACK){
if (board[a-1][b-1].type == NONE){

}else {
if (board[a-1][b-1].type == WHITE)
if (board[a-2][b-2].type == NONE)
printf("%d,%d\n",a-2, b-2 );
else {
if (board[a-1][b-1].type == WHITE)
if (board[a-2][b-2].type == WHITE)
if (board[a-3][b-3].type == NONE)
printf("%d, %d\n",a-3, b-3 );

} } }

if (board[a][b].type == BLACK){
if (board[a+1][b-1].type == NONE){

}else {
if (board[a+1][b-1].type == WHITE)
if (board[a+2][b-2].type == NONE)
printf("%d,%d\n",a+2, b-2 );
else {
  if (board[a+1][b-1].type == WHITE)
  if (board[a+2][b-2].type == WHITE)
  if (board[a+3][b-3].type == NONE)
  printf("%d, %d\n",a+3, b-3 );

} } } } } }

    int choiceX, choiceY;
    printf("Enter where you would like to move:\n");
    scanf("%d", &choiceX);
    scanf("%d", &choiceY);
    board[choiceX][choiceY].type = 1;


    //Only directions which work for black that don't cause stray "1's"
    //The more functions that were added thus the more complex it became,
    //the more the program actually became faulty.
    //Truth values are used to validate a move function movement.
    if((LEFT_BLACK(choiceX, choiceY, board) == true)){
            makeMoveWhite(board);

    }else if((RIGHT_BLACK(choiceX, choiceY, board) == true)){
            updateBoard(choiceX, choiceY, board);
            makeMoveWhite(board);

    }else if((DOWN_BLACK(choiceX, choiceY, board) == true)){
            updateBoard(choiceX, choiceY, board);
            makeMoveWhite(board);
    }
}

//Essentially resued code for position white.
void makeMoveWhite(disk board[SIZE][SIZE]){
  int a, b , i, j ;

  printf("\nAvailable Moves for player 2:\n");
  for (i = 0;i<SIZE; i++){
    for (j=0; j<SIZE; j++){
      switch(board[i][j].type ){
        case NONE:
        break;

        case WHITE:
        a = i;
        b = j;

            if (board[a][b].type == WHITE){
              if (board[a-1][b].type == NONE){

            }else {
                if (board[a-1][b].type == BLACK)
                if (board[a-2][b].type == NONE)
                printf("%d,%d\n",a-2, b );
                else {
                  if (board[a-1][b].type == BLACK)
                  if (board[a-2][b].type == BLACK)
                  if (board[a-3][b].type == NONE)
                  printf("%d, %d\n",a-3, b );

} } }

if (board[a][b].type == WHITE){
  if (board[a][b+1].type == NONE){
}else {
    if (board[a][b+1].type == BLACK)
    if (board[a][b+2].type == NONE)
    printf("%d,%d\n",a, b+2 );
    else {
      if (board[a][b+1].type == BLACK)
      if (board[a][b+2].type == BLACK)
      if (board[a][b+3].type == NONE)
      printf("%d, %d\n",a, b+3);

} } }
if (board[a][b].type == WHITE){
  if (board[a+1][b].type == NONE){

}else {
    if (board[a+1][b].type == BLACK)
    if (board[a+2][b].type == NONE)
    printf("%d,%d\n",a+2, b );
    else {
      if (board[a+1][b].type == BLACK)
      if (board[a+2][b].type == BLACK)
      if (board[a+3][b].type == NONE)
      printf("%d, %d\n",a+3, b );
} } }

if (board[a][b].type == WHITE){
  if (board[a][b-1].type == NONE){

}else {
    if (board[a][b-1].type == BLACK)
    if (board[a][b-2].type == NONE)
    printf("%d,%d\n",a, b-2 );
    else {
      if (board[a][b-1].type == BLACK)
      if (board[a][b-2].type == BLACK)
      if (board[a][b-3].type == NONE)
      printf("%d, %d\n",a, b-3 );
} } }

if (board[a][b].type == WHITE){
  if (board[a-1][b+1].type == NONE){

}else {
    if (board[a-1][b+1].type == BLACK)
    if (board[a-2][b+2].type == NONE)
    printf("%d,%d\n",a-2, b+2 );
    else {
      if (board[a-1][b+1].type == BLACK)
      if (board[a-2][b+2].type == BLACK)
      if (board[a-3][b+3].type == NONE)
      printf("%d, %d\n",a-3, b+3 );
} } }

if (board[a][b].type == WHITE){
if (board[a+1][b+1].type == NONE){

}else {
if (board[a+1][b+1].type == BLACK)
if (board[a+2][b+2].type == NONE)
printf("%d,%d\n",a+2, b+2 );
else {
if (board[a+1][b+1].type == BLACK)
if (board[a+2][b+2].type == BLACK)
if (board[a+3][b+3].type == NONE)
printf("%d, %d\n",a+3, b+3 );
} } }

if (board[a][b].type == WHITE){
if (board[a-1][b-1].type == NONE){

}else {
if (board[a-1][b-1].type == BLACK)
if (board[a-2][b-2].type == NONE)
printf("%d,%d\n",a-2, b-2 );
else {
if (board[a-1][b-1].type == BLACK)
if (board[a-2][b-2].type == BLACK)
if (board[a-3][b-3].type == NONE)
printf("%d, %d\n",a-3, b-3 );
} } }

if (board[a][b].type == WHITE){
if (board[a+1][b-1].type == NONE){

}else {
if (board[a+1][b-1].type == BLACK)
if (board[a+2][b-2].type == NONE)
printf("%d,%d\n",a+2, b-2 );
else {
  if (board[a+1][b-1].type == BLACK)
  if (board[a+2][b-2].type == BLACK)
  if (board[a+3][b-3].type == NONE)
  printf("%d, %d\n",a+3, b-3 );

}
}
}
	  }
	}
  }


    int choiceX, choiceY;
    printf("Enter where you would like to move:\n");
    scanf("%d", &choiceX);
    scanf("%d", &choiceY);
    board[choiceX][choiceY].type = 0;

     //Only directions which work for black that don't cause stray "0's"
    if((LEFT_WHITE(choiceX, choiceY, board) == true)){
                        updateBoard(choiceX, choiceY, board);


    }else if((RIGHT_WHITE(choiceX, choiceY, board) == true)){
            updateBoard(choiceX, choiceY, board);


    }else if((DOWN_WHITE(choiceX, choiceY, board) == true)){
            updateBoard(choiceX, choiceY, board);
            }
}

void updateBoard(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    int i;
    printf("\n    ");
    for(i  = 0; i < SIZE; i++){
        printf("%d   ", i);
    }

    for(choiceX = 0; choiceX < SIZE; choiceX++){
        printf("\n%d | ", choiceX);
        for(choiceY = 0; choiceY < SIZE; choiceY++){
            if(board[choiceX][choiceY].type == 1){
                    printf("1 | ");
                    }else if(board[choiceX][choiceY].type == 0){
                    printf("0 | ");
                    }else{
                    printf("x | ");
                    }
        }
    }
}
int RIGHT_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]){
        //Chnages values to right.
        choiceY = choiceY;
        for(choiceX = choiceX; choiceX < SIZE; choiceX++){
            if(board[choiceX][choiceY].type == 1){

                    for(choiceX = choiceX; choiceX < (SIZE - choiceX); choiceX++){
                            board[choiceX][choiceY].type = 1;
                    }
            }
        }
        updateBoard(choiceX, choiceY, board);
}


int LEFT_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]){

        //Changes values to left
        choiceX = choiceX;
        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY--){
            if(board[choiceX][choiceY].type == 1){
                    for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY--){
                            board[choiceX][choiceY].type = 1 == true;
                    }
            }
        }
        updateBoard(choiceX, choiceY, board);
}

int UP_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]){

  for(choiceX = choiceX; choiceX > 0; choiceX--){
            if(board[choiceX][choiceY].type == 1){
                for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
                    board[choiceX][choiceY].type = 1 == true;
                }
            }

    }
    updateBoard(choiceX, choiceY, board);
}

int DOWN_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    //Changes values down a column
    for(choiceX = choiceX; choiceX < SIZE; choiceX++){
            if(board[choiceX][choiceY].type == 1){
                for(choiceX = choiceX; choiceX < (SIZE - choiceX); choiceX++){
                    board[choiceX][choiceY].type = 1 == true;
                }
            }
    }
    updateBoard(choiceX, choiceY, board);
}

int DIAGONAL_BLACK_TOP_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
        for(choiceY = choiceY; choiceY < SIZE; choiceY++){
            if(board[choiceX][choiceY].type == 1){
                for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
                    for(choiceY = choiceY; choiceY < SIZE; choiceY++){
                        board[choiceX][choiceY].type = 1 == true;
                    }
                }
            }
        }
    }


}

int DIAGONAL_BLACK_BOTTOM_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX < SIZE; choiceX++){
        for(choiceY = choiceY; choiceY < SIZE; choiceY++){
            if(board[choiceX][choiceY].type == 1){
                for(choiceX = choiceX; choiceX < SIZE; choiceX++){
                    for(choiceY = choiceY; choiceY < SIZE; choiceY++){
                        board[choiceX][choiceY].type = 1;
                    }
                }
            }
        }
    }
}

int DIAGONAL_BLACK_BOTTOM_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX < SIZE; choiceX++){
        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY--){
            if(board[choiceX][choiceY].type == 1){
                for(choiceX = choiceX; choiceX < SIZE; choiceX++){
                    for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY--){
                        board[choiceX][choiceY].type = 1;
                    }
                }
            }
        }
    }
}

int DIAGONAL_BLACK_TOP_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY++){
            if(board[choiceX][choiceY].type == 1){
                    for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
                        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY++){
                            board[choiceX][choiceY].type = 1;
                    }
                }
            }
        }
    }
}

int RIGHT_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]){

        for(choiceY = choiceY; choiceY < SIZE; choiceY++){
            if(board[choiceX][choiceY].type == 0){
                    for(choiceY = choiceY; choiceY < (SIZE - choiceY); choiceY++){
                            board[choiceX][choiceY].type = 0 == true;
                    }
            }
        }
        updateBoard(choiceX, choiceY, board);
        makeMove(board);
}

int LEFT_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceY = choiceY; choiceY > 0; choiceY--){
            if(board[choiceX][choiceY].type == 0){
                    for(choiceY = choiceY; choiceY > (SIZE - choiceY); choiceY++){
                            board[choiceX][choiceY].type = 0 == true;
                    }
            }
    }
    updateBoard(choiceX, choiceY, board);
    makeMove(board);
}

int UP_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX > 0; choiceX--){
            if(board[choiceX][choiceY].type == 0){
                for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
                    board[choiceX][choiceY].type = 0 == true;
                }
            }

    }
    updateBoard(choiceX, choiceY, board);
}

int DOWN_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX < SIZE; choiceX++){
            if(board[choiceX][choiceY].type == 0){
                for(choiceX = choiceX; choiceX < (SIZE - choiceX); choiceX++){
                    board[choiceX][choiceY].type = 0 == true;
                }
            }

    }
    updateBoard(choiceX, choiceY, board);
    makeMoveWhite(board);
}

int DIAGONAL_WHITE_TOP_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
        for(choiceY = choiceY; choiceY < SIZE; choiceY++){
            if(board[choiceX][choiceY].type == 0){
                for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
                    for(choiceY = choiceY; choiceY < SIZE; choiceY++){
                        board[choiceX][choiceY].type = 0 == true;
                    }
                }
            }
        }
    }
}

int DIAGONAL_WHITE_BOTTOM_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX < SIZE; choiceX++){
        for(choiceY = choiceY; choiceY < SIZE; choiceY++){
            if(board[choiceX][choiceY].type == 0){
                for(choiceX = choiceX; choiceX < SIZE; choiceX++){
                    for(choiceY = choiceY; choiceY < SIZE; choiceY++){
                        board[choiceX][choiceY].type = 0;
                    }
                }
            }
        }
    }
}

int DIAGONAL_WHITE_BOTTOM_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX < SIZE; choiceX++){
        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY--){
            if(board[choiceX][choiceY].type == 0){
                for(choiceX = choiceX; choiceX < SIZE; choiceX++){
                    for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY--){
                        board[choiceX][choiceY].type = 0;
                    }
                }
            }
        }
    }
}

int DIAGONAL_WHITE_TOP_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]){
    for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY++){
            if(board[choiceX][choiceY].type == 0){
                    for(choiceX = choiceX; choiceX > (SIZE - choiceX - choiceX); choiceX--){
                        for(choiceY = choiceY; choiceY > (SIZE - choiceY - choiceY); choiceY++){
                            board[choiceX][choiceY].type = 0;
                    }
                }
            }
        }
    }
}

























