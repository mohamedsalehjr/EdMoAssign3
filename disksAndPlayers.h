
    #define SIZE 8

    enum colour{
        WHITE,
        BLACK,
        NONE
    };

    typedef struct position{
        int row;
        int col;
    } position;

    typedef struct disk{
        enum colour type;
        position pos;

    } disk;

    typedef struct player{
        char name[20];
        enum colour type;
        int points;
    } player;

    void initializePlayers(player player1, player player2);

    void initializeBoard(disk board[SIZE][SIZE]);

    void printBoard(disk board [SIZE][SIZE]);

    void makeMove(disk board[SIZE][SIZE]);
    void makeMoveWhite(disk board[SIZE][SIZE]);

    void updateBoard(int choiceX, int choiceY, disk board[SIZE][SIZE]);

    //All game logic functions.
    int RIGHT_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int LEFT_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int UP_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DOWN_BLACK(int choiceX, int choiceY, disk board[SIZE][SIZE]);

    int RIGHT_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int LEFT_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int UP_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DOWN_WHITE(int choiceX, int choiceY, disk board[SIZE][SIZE]);

    int DIAGONAL_BLACK_TOP_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DIAGONAL_BLACK_BOTTOM_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DIAGONAL_BLACK_TOP_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DIAGONAL_BLACK_BOTTOM_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]);

    int DIAGONAL_WHITE_TOP_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DIAGONAL_WHITE_BOTTOM_RIGHT(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DIAGONAL_WHITE_TOP_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]);
    int DIAGONAL_WHITE_BOTTOM_LEFT(int choiceX, int choiceY, disk board[SIZE][SIZE]);

