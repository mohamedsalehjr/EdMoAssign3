Division of Work:

I focused mainly on the game logic. We used Mohamed's "move finder" as it was much more efficient than mine,
and mine had a problem of printing the same available move repeatdly.


Implemention of Game Logic:

I made numerous tests of how to make the game logic work.
I did this by creating different files. 
In these files I used sample arrays to obtain a correct logical structure for moves in any direction.
Mohamed helped me implement the different moves into the game as we encountered issues.

Issues:

The issues we encountered seemed to come from increasing complexity with the addition of more functions.
In some instances for example, we found that there was an extra '1' or '0' printed or nothing would happen or in the submitted version,
mulitple boards are printed when the user inputs a move. Due to this we included only a few functions that we felt worked best. In my 
personal attempts to correct the multiple board problem, I found that the issue was interlinked with the extra values. I think that the quantity of times 
it was printed influenced the extra value but I can not confirm this for definite. 

Thus due to the nature of the issues we had, we did not include score managment as we came to a mutual decision that it would be better
to have more move functionality that worked than the possibility of nothing at all. 



