#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "disksAndPlayers.h"



int main()
{
    //Moodle Code
    player player1= {"player1", NONE,0};
    player player2= {"player2", NONE,0};
    disk board[SIZE][SIZE];
    initializePlayers(player1, player2);
    initializeBoard(board);
    printBoard(board);
    
    //Own code
    makeMove(board);
    makeMoveWhite(board);

    return 0;
}
